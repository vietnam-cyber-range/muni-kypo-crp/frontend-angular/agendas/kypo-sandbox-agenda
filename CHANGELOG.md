### 16.0.4 Adjust styling of pool overview components.
* 06b313a -- [CI/CD] Update packages.json version based on GitLab tag.
* db338a8 -- Adjust styling of pool overview components
### 16.0.3 Fix and unify acquisition of sandbox ids and pool deletion.
* fe08edf -- [CI/CD] Update packages.json version based on GitLab tag.
* 7ed11ff -- Fix and unify acquisition of sandbox ids and pool deletion
### 16.0.2 Add an extended graphical view for pool overview, enable sending notifications upon sandbox building.
* 69a7d93 -- [CI/CD] Update packages.json version based on GitLab tag.
* 8981253 -- Merge branch '129-add-checkbox-for-pool-email-notifications' into 'master'
* 13c49db -- Adjust edit view for pools, export resources component
* df2205f -- Add graphical resources to pool table
* 5af5a4c -- Adjust pool table to show expanded content
### 16.0.1 Enable a force deletion of pools with allocations.
* f7657a2 -- [CI/CD] Update packages.json version based on GitLab tag.
* 4ee518b -- Merge branch '128-enable-force-delete-of-pools-with-allocations' into 'master'
*   d0eb662 -- Merge branch '128-enable-force-delete-of-pools-with-allocations' of gitlab.ics.muni.cz:muni-kypo-crp/frontend-angular/agendas/kypo-sandbox-agenda into 128-enable-force-delete-of-pools-with-allocations
|\  
| * e2c1c4c -- Enable a force deletion of pools with allocations
* bffbbf6 -- Enable a force deletion of pools with allocations
### 16.0.0 Update to Angular 16, add keycloak, introduce pool and sandbox comments, enable pool edition.
* edc2a06 -- [CI/CD] Update packages.json version based on GitLab tag.
* 5533e81 -- Merge branch '126-add-an-option-to-comment-on-pools-and-sandbox-instances' into 'master'
* c8d58c7 -- Merge branch 'master' into '126-add-an-option-to-comment-on-pools-and-sandbox-instances'
* f91049f -- Fix library build
* 028b9f2 -- Add keycloak as a substitute for local issuer, fix lint
* a61938c -- Add comment display and edit form to individual sandboxes and pools, update to Angular 16
* aec15e9 -- Add comment display in pool detail
* e74a694 -- Refactor pool editation constructor
* 410da9c -- Add comments column with pool editation form
### 15.2.0 Update sandbox definition placeholder url to http format.
* 3204c17 -- [CI/CD] Update packages.json version based on GitLab tag.
* 79e904b -- Merge branch 'develop' into 'master'
* 8a34271 -- Merge branch 'master' into 'develop'
* 244b780 -- Merge branch '127-update-sandbox-definition-placeholder-url-to-https' into 'develop'
* 8552cc0 -- update url variable names
* 0baeb78 -- Change sandbox definition url placeholder to https
### 15.1.3 Adjust and specify sandbox allocation label and tooltip.
* fc12ddd -- [CI/CD] Update packages.json version based on GitLab tag.
* f96829c -- Merge branch '125-adjust-pool-allocation-tooltip' into 'master'
* d0bf8b7 -- Update version
* 23502eb -- Adjust allocation text in pool agenda
### 15.1.2 Update sandbox api to accept paginated allocation units.
* 0e7a7cf -- [CI/CD] Update packages.json version based on GitLab tag.
* 2857338 -- Update sandbox api version
### 15.1.1 Fix sandbox definition faulty buttons.
* d0df861 -- [CI/CD] Update packages.json version based on GitLab tag.
* dbc8dc7 -- Merge branch 'develop' into 'master'
* 497a2b3 -- Merge branch 'fix-button' into 'develop'
* a30e9fe -- Fix faulty buttons
### 15.1.0 Adjust pool view to communicate with optimized sandbox service.
* 21b5986 -- [CI/CD] Update packages.json version based on GitLab tag.
*   805c1fe -- Merge branch 'develop' into 'master'
|\  
| *   0586c46 -- Merge branch 'master' into 'develop'
| |\  
| |/  
|/|   
* | 95bc7c0 -- Merge branch '124-update-communication-with-optimized-sandbox-service' into 'master'
* | af233f1 -- Adjust pool view to communicate with optimized sandbox service
 /  
*   1386200 -- Merge branch '119-add-network-and-ports-resources-to-pool-table' into 'develop'
|\  
| *   56dae45 -- Merge branch 'develop' into '119-add-network-and-ports-resources-to-pool-table'
| |\  
| |/  
|/|   
* |   ed3e4ab -- Merge branch '121-move-revision-column-to-title' into 'develop'
|\ \  
| * \   a073eb2 -- Merge develop, update sandbox definition column
| |\ \  
| |/ /  
|/| |   
| * | 561fc3a -- Update version
| * | 0d995fe -- Bump version
| * | fc53e6f -- Merge remote-tracking branch 'origin/develop' into 121-move-revision-column-to-title
| * | 77eb039 -- Move revision to sandbox definition title and remove revision column
|  /  
| * b61f251 -- Update version string
| *   987acf2 -- Merge develop
| |\  
| |/  
|/|   
* | 0cdba6e -- Merge branch into 'develop'
 /  
* e08f02c -- Update version
* 8d483c9 -- bump version
* ddd7d26 -- Add ports and networks usage to pool table
### 15.0.1 Update agenda cards styling.
* 993104f -- [CI/CD] Update packages.json version based on GitLab tag.
* b5c38bf -- Merge branch '123-add-padding-to-new-material-cards' into 'master'
* 566361a -- Update card styling
### 15.0.0 Update to Angular 15.
* 0829601 -- [CI/CD] Update packages.json version based on GitLab tag.
* e5a9a03 -- Merge branch '117-refactor-the-sandbox-allocation-calls' into 'master'
*   05c245a -- Merge branch '117-refactor-the-sandbox-allocation-calls' of gitlab.ics.muni.cz:muni-kypo-crp/frontend-angular/agendas/kypo-sandbox-agenda into 117-refactor-the-sandbox-allocation-calls
|\  
| *   a00d3ab -- Merge branch 'develop' into '117-refactor-the-sandbox-allocation-calls'
| |\  
| | * fa685d2 -- Merge branch '118-move-branch-from-sandbox-definition-detail-to-column' into 'develop'
| | * fdf9104 -- bump version.
| | * b34b30c -- Remove column revision.
| | * 3b5117d -- Add revision column and Title modification.
| *   87f4f66 -- Merge allocation process
| |\  
* | \   5d54ea5 -- Merge allocation process
|\ \ \  
| |/ /  
|/| /   
| |/    
| * bfbe867 -- Update to Angular 15
| * d1630af -- Refactor the pool allocation calls
* 529cb80 -- Revert allocation merge
* bb31f6f -- Refactor the pool allocation calls
### 14.4.7 Fix the retrieval of poolId upon a single sandbox allocation.
* e07c548 -- [CI/CD] Update packages.json version based on GitLab tag.
* f8140b0 -- Merge branch '116-allocating-a-single-sandbox-returns-error' into 'master'
* 18922bd -- Fix poolId retrieval for sandbox allocation
### 14.4.6 Add sorting options for pool, sandbox definition and resources tables.
* 20b199c -- [CI/CD] Update packages.json version based on GitLab tag.
* 8e61c02 -- Merge branch '115-add-relevant-sorting-options-for-tables' into 'master'
* d721d2b -- Update version.
* b7b1f7d -- Revert resources table sorting change.
* e69ff9f -- Add sorting options for pool, sandbox definition and resources tables.
### 14.4.5 Add option to confirm sandbox definition creation by pressing enter and added whitespace trimming. Added ports and networks to resources. Added revision to pool overview. Enhanced sandbox allocation confirmation dialog. Disabled lock option when allocation fails.
* f2ecb72 -- [CI/CD] Update packages.json version based on GitLab tag.
* ec782e7 -- Merge branch 'develop' into 'master'
*   1064b12 -- Merge branch '108-trimming-of-whitespaces-in-sandbox-definition-create' into 'develop'
|\  
| * 13b08e9 -- Resolve "trimming of whitespaces in sandbox definition create"
* |   37a9a3e -- Merge branch '112-disable-lock-button-when-topology-allocation-fails' into 'develop'
|\ \  
| * | d2e0e92 -- Resolve "Disable lock button when topology allocation fails"
| |/  
* |   17c745d -- Merge branch '114-enhancement-of-sandbox-detail-allocation-dialog' into 'develop'
|\ \  
| * | db458f0 -- Resolve "Enhancement of sandbox detail allocation dialog"
| |/  
* |   a39b55f -- Merge branch '113-add-revision-to-pool-overview' into 'develop'
|\ \  
| * | cbcda49 -- Resolve "Add revision to pool overview"
| |/  
* |   620f468 -- Merge branch '111-add-ports-and-networks-to-resources' into 'develop'
|\ \  
| |/  
|/|   
| * 6d3e357 -- Resolve "Add ports and networks to resources"
* e93b876 -- Merge branch '109-adding-option-to-confirm-sd-creation-by-pressing-enter' into 'develop'
* 56a9b4f -- Resolve "Adding option to confirm SD creation by pressing 'enter'."
### 14.4.4 Update topology view with new legend
* 25099bb -- [CI/CD] Update packages.json version based on GitLab tag.
* f3b6a8e -- Merge branch '110-update-topology-component-with-new-legend' into 'master'
* cf6bd4e -- Update with new style of topology legend display
### 14.4.3 Fix sandboxes sorting in pool detail table.
* ed38765 -- [CI/CD] Update packages.json version based on GitLab tag.
* 637400c -- Merge branch 'fix-sandboxes-sorting' into 'master'
* 0bec373 -- Bump sandbox api
### 14.4.2 Bump topology version to address fix of collapsed subnet size number.
* d92305f -- [CI/CD] Update packages.json version based on GitLab tag.
* 62b9aa3 -- Merge branch 'bump-topology-version' into 'master'
* b495a1f -- Bump topology version
### 14.4.1 Resolve problems with guacamole. Fix restrictions for the allocation dialogue and fix force delete for sandbox under allocation.
* 96561b0 -- [CI/CD] Update packages.json version based on GitLab tag.
* 769a7fc -- Merge branch 'fix-problems-found-during-release-testing' into 'master'
* 3f9488f -- Fix problems found during release testing
### 14.4.0 Added dynamic allocation of sandboxes through the dialog window with slider.
* 82bfbd9 -- [CI/CD] Update packages.json version based on GitLab tag.
* 8483d1a -- Merge branch '106-option-to-allocate-specified-number-of-sandboxes' into 'master'
* 060d966 -- Resolve "Option to allocate specified number of sandboxes"
### 14.3.1 Add option to sort the pool detail table by Create By, Name and Created.
* 0887e16 -- [CI/CD] Update packages.json version based on GitLab tag.
* 3a10fd6 -- Merge branch '102-add-sorting-options-to-the-pool-detail-table' into 'master'
* 8c22033 -- Merge branch 'master' into 102-add-sorting-options-to-the-pool-detail-table
* fdb83ec -- Change sorting of created by from id to first name and added tag message
* dd0abae -- Updated sorting table values.
* 1a6a24a -- Merge branch 'master' into 102-add-sorting-options-to-the-pool-detail-table merging latest changes from master.
* 5ab89d5 -- added sorting options for several columns in pool-detail-table
### 14.3.0 Replace sandbox id with sandbox uuid.
* f7af18f -- [CI/CD] Update packages.json version based on GitLab tag.
* b92dba2 -- Merge branch '104-change-sandbox-id-to-uuid-and-use-string-data-type' into 'master'
* a9653e5 -- Resolve "Change sandbox id to uuid and use string data type"
### 14.2.1 Change delete allocation units buttons to one expandable button with options to delete all, unlocked and failed.
* 1136845 -- [CI/CD] Update packages.json version based on GitLab tag.
*   388c034 -- Merge branch '103-in-the-pool-detail-page-add-delete-all-unlocked-button' into 'master'
|\  
| * d238f23 -- Resolve "In the Pool Detail page, add "Delete All Unlocked" button"
|/  
* 57cfbbb -- Merge branch '101-refactor-resources-page' into 'master'
* cb9bee4 -- Resolve "Refactor Resources page"
### 14.2.0 Refactor resources page. Add information about GUI access and version to image. Rearrange information in image column and detail. Add checkbox to filter images with GUI access. Add Header to Images table.
* 5a6b2d5 -- [CI/CD] Update packages.json version based on GitLab tag.
* a24af56 -- Merge branch '101-refactor-resources-page' into 'master'
* 54ae6bb -- Resolve "Refactor Resources page"
### 14.1.3 Address changes in delete failed, unlocked and all allocation units.
* a088278 -- [CI/CD] Update packages.json version based on GitLab tag.
* 1b539f2 -- Merge branch '105-address-cleanup-multiple-endpoint-change' into 'master'
* 3605ec5 -- Resolve "Address cleanup multiple endpoint change"
### 14.1.2 Fix polling problem caused by pagination setting.
* 8fd2ebc -- [CI/CD] Update packages.json version based on GitLab tag.
* 10342d4 -- Merge branch 'fix-polling' into 'master'
* 4edf98c -- Fix polling
### 14.1.1 Fix problem with delete action of sandbox instance at pool detail table.
* e4021a8 -- [CI/CD] Update packages.json version based on GitLab tag.
* 3bc240e -- Merge branch 'fix-sandbox-allocation-unit-delete' into 'master'
* 5b0967f -- Fix sandbox allocation unit delete
### 14.1.0 Optimize calls, polling and move spice console loading to topology.
* f538996 -- [CI/CD] Update packages.json version based on GitLab tag.
*   c81af15 -- Merge branch '100-disable-spice-console-when-it-is-not-ready' into 'master'
|\  
| * 6abf4c5 -- Resolve "Disable spice console when it is not ready"
|/  
* cd5e512 -- Merge branch '97-optimize-api-calls' into 'master'
* f875e0d -- Resolve "Optimize API calls"
### 14.0.1 Rename from kypo2 to kypo.
* 6fe346b -- [CI/CD] Update packages.json version based on GitLab tag.
* e6af314 -- Merge branch '98-rename-from-kypo2-to-kypo' into 'master'
* 25f5faa -- Resolve "Rename from kypo2 to kypo"
### 14.0.0 Update to Angular 14.
* a2c4b69 -- [CI/CD] Update packages.json version based on GitLab tag.
* 1e049d2 -- Merge branch '96-update-to-angular-14' into 'master'
* 9f0ebfd -- Resolve "Update to Angular 14"
### 13.2.6 Bump topology graph to resolve problems with console selection.
* 2f6ec16 -- [CI/CD] Update packages.json version based on GitLab tag.
* 0bf5298 -- Merge branch '95-bump-topology-graph-patch-version' into 'master'
* 9dd07a4 -- Resolve "Bump topology graph patch version"
### 13.2.5 Improve images table.
* d3de7a6 -- [CI/CD] Update packages.json version based on GitLab tag.
* 6336751 -- Merge branch '94-improve-the-images-list' into 'master'
* 76e326d -- Resolve "Improve the images list"
### 13.2.4 Show additional actions in tables.
* 38d3ad5 -- [CI/CD] Update packages.json version based on GitLab tag.
* 010437a -- Merge branch '92-show-additional-actions-in-tables' into 'master'
* a869435 -- Resolve "Show additional actions in tables"
### 13.2.3 Bump version of topology graph
* 47bdb1a -- [CI/CD] Update packages.json version based on GitLab tag.
*   b4859aa -- Merge branch '91-bump-version-of-topology-graph' into 'master'
|\  
| * 760a501 -- Resolve "Bump version of topology  graph"
|/  
* 162899f -- Merge branch '90-fix-pagination-for-sandbox-instances' into 'master'
* 9457d9b -- Resolve "Fix pagination for sandbox instances"
### 13.2.2 Remove pool size max limitation
* a9e3595 -- [CI/CD] Update packages.json version based on GitLab tag.
* 09b456c -- Merge branch '89-remove-maximal-pool-size' into 'master'
* 8e046fb -- Resolve "Remove maximal pool size"
### 13.2.1 Fix table reload on pagination change
* 65feaad -- [CI/CD] Update packages.json version based on GitLab tag.
* 68c8365 -- Merge branch '88-fix-table-reload-on-pagination-change' into 'master'
* e5112d4 -- Resolve "Fix table reload on pagination change"
### 13.2.0 Change openstack allocation stage to terraform and adjust stage information. Fix retry stage for pool overview. Add redirect to stage detail upon selecting it in pipeline.
* c5191fe -- [CI/CD] Update packages.json version based on GitLab tag.
* 138ea29 -- Merge branch '86-replace-openstack-with-terraform-and-add-separate-stage-view' into 'master'
* 999501e -- Resolve "Replace openstack with terraform and add separate stage view"
### 13.1.0 Add display topology option for sandbox definitions in sandbox definition overview
* 49f8332 -- [CI/CD] Update packages.json version based on GitLab tag.
* 2bf7e38 -- Merge branch '87-button-to-display-topology-in-sandbox-definition-overview' into 'master'
* 0fc6f5b -- Display topology of the sandbox definition.
### 13.0.2 Fix pool creation pagination size for sandbox definition select
* 22f3e58 -- [CI/CD] Update packages.json version based on GitLab tag.
*   be266ea -- Merge branch '13.0.2-release-tag' into 'master'
|\  
| * 4e4c467 -- Tag message
|/  
* 407060d -- Merge branch '85-in-pool-creation-fix-pagination-size-for-sandbox-definition-select' into 'master'
* 139369b -- Resolve "In pool creation fix pagination size for sandbox definition select"
### 13.0.1 Fix peer dependencies of package
* 1285bbc -- [CI/CD] Update packages.json version based on GitLab tag.
* e3c2ad4 -- Merge branch 'fix-library-peerdependencies' into 'master'
* fc132a8 -- Fix peer dependencies
### 13.0.0 Update to Angular 13, CI/CD optimization, retry of allocation pipeline on fail supported, revision and id of SD added to pool creation
* a86c690 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7d03735 -- Merge branch '13.0.0-release-tag' into 'master'
|\  
| * a1bcdb2 -- Tag message
|/  
*   8165cc9 -- Merge branch '83-update-to-angular-13' into 'master'
|\  
| * 01fbe6d -- Resolve "Update to Angular 13"
|/  
*   906099b -- Merge branch '81-add-retry-to-option-to-allocation-pipeline-on-fail' into 'master'
|\  
| * 2752444 -- Resolve "Add retry to option to allocation pipeline on fail"
|/  
*   c237a62 -- Merge branch '82-add-revision-and-id-to-sandbox-definition-in-pool-creation' into 'master'
|\  
| * 5783676 -- Resolve "Add revision and id to sandbox definition in pool creation"
|/  
* 17d2aa0 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8e05424 -- [CI/CD] Update packages.json version based on GitLab tag.
* 009dff0 -- Merge branch '80-fix-pagination' into 'master'
* 5f1c79a -- Resolve "Fix pagination"
### 12.0.10 Fix pagination
* 8e05424 -- [CI/CD] Update packages.json version based on GitLab tag.
* 009dff0 -- Merge branch '80-fix-pagination' into 'master'
* 5f1c79a -- Resolve "Fix pagination"
### 12.0.9 Add support for backend sort
* b98c84c -- [CI/CD] Update packages.json version based on GitLab tag.
* f350925 -- Merge branch '79-add-support-for-backend-sort' into 'master'
* 06bfc61 -- Resolve "Add support for backend sort"
### 12.0.8 Add sandbox definition name to pool overview
* c649a4b -- [CI/CD] Update packages.json version based on GitLab tag.
* 0af4793 -- Merge branch '78-add-sandbox-definition-name-to-pool-overview' into 'master'
* 4cfb50d -- Resolve "Add sandbox definition name to pool overview"
### 12.0.7 Preload spice in topology. Add created by field to sandbox definition.
* 3117f5b -- [CI/CD] Update packages.json version based on GitLab tag.
* 460d34d -- Merge branch '77-preload-spice-in-display-topology' into 'master'
* 4f4155a -- Resolve "Preload spice in display topology"
### 12.0.6 Redesign detail page for pools
* ef11bbd -- [CI/CD] Update packages.json version based on GitLab tag.
*   4e1f897 -- Merge branch '68-redesign-detail-page-for-pools' into 'master'
|\  
| * ed20708 -- Resolve "Redesign detail page for pools"
|/  
*   37cac58 -- Merge branch '76-bump-d3-version' into 'master'
|\  
| * fe72019 -- Bump d3 version
|/  
*   3a6b293 -- Merge branch '75-add-license-file' into 'master'
|\  
| * fe4e7b2 -- Add license file
|/  
* fa0dd7d -- Merge branch '74-bump-version-of-sentinel' into 'master'
* 886c88f -- Bump sentinel
### 12.0.5 New version of the topology graph package - added missing configuration for Apache Guacamole.
* 998ea43 -- [CI/CD] Update packages.json version based on GitLab tag.
* 46051cc -- Merge branch '73-update-version-of-the-topology-graph-package' into 'master'
* 54e3773 -- New version of the topology graph pacakge.
### 12.0.4 Add build of example app to CI. Rename endpoint from kypo to kypo. Save user preferred pagination. Change titles for SSH buttons. Add aliases to tsconfig.
* 320fcfc -- [CI/CD] Update packages.json version based on GitLab tag.
*   b402c02 -- Merge branch '72-create-tag-with-latest-changes' into 'master'
|\  
| * 02d2e20 -- Resolve "Create tag with latest changes"
|/  
*   dc3fa11 -- Merge branch '70-add-node-modules-to-gitignore' into 'master'
|\  
| * 5976208 -- add node modules to gitignore
* |   8fa1ae7 -- Merge branch '71-add-link-to-sandbox-definition-repository' into 'master'
|\ \  
| |/  
|/|   
| * a174321 -- Resolve "Add link to sandbox definition repository"
|/  
*   2c112f9 -- Merge branch '69-add-aliases-to-tsconfig' into 'master'
|\  
| * b0e1c1c -- Added aliases to tsconfig
|/  
*   ea6be9d -- Merge branch '64-rename-button-for-downloading-ssh-access' into 'master'
|\  
| * 3a1a8b0 -- Resolve "Rename button for downloading SSH access"
|/  
*   853d0f3 -- Merge branch '66-update-name-for-download-ssh-access' into 'master'
|\  
| * 268b8f0 -- Update download access button
|/  
*   18d7c43 -- Merge branch '64-rename-button-for-downloading-ssh-access' into 'master'
|\  
| * 6750213 -- Resolve "Rename button for downloading SSH access"
|/  
*   303a040 -- Merge branch '62-save-user-preferences-of-pagination-page-size' into 'master'
|\  
| * 1b794e4 -- Resolve "Save user preferences of pagination page size"
|/  
*   05b4d37 -- Merge branch '65-bump-version-of-sentinel' into 'master'
|\  
| * 4791e2f -- Bump version of Sentinel
|/  
*   13dc6a5 -- Merge branch '63-fix-local-config-paths' into 'master'
|\  
| * 8c65842 -- Fix paths
|/  
* 5a4a2a8 -- Merge branch '61-add-build-example-app-to-ci' into 'master'
* 908a7bd -- Add build example app to CI
### 12.0.3 Update gitlab CI
* 1300806 -- [CI/CD] Update packages.json version based on GitLab tag.
*   6606cca -- Merge branch '60-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 96aef96 -- Update gitlab CI
|/  
* 1261f05 -- Update project package.json version based on GitLab tag. Done by CI
*   cb8688a -- Merge branch '59-bump-sentinel-layout-version' into 'master'
|\  
| * 93cd73c -- Resolve "Bump sentinel layout version"
|/  
* fd49249 -- Update project package.json version based on GitLab tag. Done by CI
*   cb3d60d -- Merge branch '58-bump-version-of-sentinel' into 'master'
|\  
| * b930ca2 -- Resolve "Bump version of Sentinel"
|/  
* 482865f -- Update project package.json version based on GitLab tag. Done by CI
*   6d3e324 -- Merge branch '57-update-to-angular-12' into 'master'
|\  
| * 2726c21 -- Resolve "Update to Angular 12"
|/  
*   3194ee9 -- Merge branch '55-update-oidc-configuration' into 'master'
|\  
| * ca27ad3 -- Resolve "Update oidc configuration"
|/  
* 9bb31fd -- Update project package.json version based on GitLab tag. Done by CI
*   f0bdb10 -- Merge branch '54-update-to-angular-11' into 'master'
|\  
| * 7339c17 -- Resolve "Update to Angular 11"
|/  
*   a61f272 -- Merge branch '53-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * d3b3363 -- recreate package
|/  
*   d882f0b -- Merge branch '52-migrate-from-tslint-to-eslint' into 'master'
|\  
| * d727025 -- Resolve "Migrate from tslint to eslint"
|/  
* 74e61f1 -- Update project package.json version based on GitLab tag. Done by CI
*   ce4b8c4 -- Merge branch '51-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 51b574c -- Rename package scope
|/  
* 67062f6 -- Update project package.json version based on GitLab tag. Done by CI
*   7d24307 -- Merge branch '50-update-dependencies-to-new-format' into 'master'
|\  
| * 1dd0d9c -- Update dependencies
|/  
* 7001908 -- Update project package.json version based on GitLab tag. Done by CI
*   8420473 -- Merge branch '49-rename-package-to-kypo-sandbox-agenda' into 'master'
|\  
| * 838474f -- Resolve "Rename package to @kypo/sandbox-agenda"
|/  
*   1914b1d -- Merge branch '48-add-tests-for-services' into 'master'
|\  
| * 2a1599c -- Resolve "Add tests for services"
|/  
*   ee02725 -- Merge branch '47-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * b4b2646 -- Remove personal info from README
|/  
*   f1f7ba5 -- Merge branch '46-replace-kypo-auth-with-sentinel-auth-for-example-app' into 'master'
|\  
| * f7b425a -- Replace kypo-auth with @sentinel/auth in example app
|/  
*   be942c1 -- Merge branch '44-no-error-message-when-getting-resources-info-fails' into 'master'
|\  
| * 809e621 -- Add error message
|/  
* a725db8 -- Update project package.json version based on GitLab tag. Done by CI
*   903fd78 -- Merge branch '45-fix-import-for-image-row-adapter' into 'master'
|\  
| * 909499b -- Fix import for image row adapter
|/  
* d453587 -- Update project package.json version based on GitLab tag. Done by CI
*   fdfa670 -- Merge branch '43-display-available-vm-images-in-resources' into 'master'
|\  
| * df17001 -- Resolve "Display available VM images in Resources"
|/  
* 912adf8 -- Update project package.json version based on GitLab tag. Done by CI
*   d77f330 -- Merge branch '29-add-sandbox-allocation-units-table-to-pool-detail-component' into 'master'
|\  
| * 456c594 -- Created table adapter, service for sandbox allocation units. Remaining to do concrete service and tests
|/  
* 3ecb969 -- Update project package.json version based on GitLab tag. Done by CI
*   98982d9 -- Merge branch '42-add-resources-to-default-paths' into 'master'
|\  
| * 9eb897b -- Resolve "Add resources to default paths"
|/  
* 76ddb35 -- Update project package.json version based on GitLab tag. Done by CI
*   fa4327c -- Merge branch '39-add-option-to-download-key-configs-and-script-in-pool-overview-table' into 'master'
|\  
| * 9fe2fc5 -- Added new download option to actions in pool overview table which will download .zip file containing script,configs and keys needed for ssh access
|/  
*   2261ce8 -- Merge branch '41-remove-unnecessary-outputs' into 'master'
|\  
| * c4f328a -- Remove unnecessary outputs
|/  
*   aeb90d3 -- Merge branch '40-add-sandbox-resource-overview' into 'master'
|\  
| * c769468 -- Resolve "Add sandbox resource overview"
|/  
*   0316237 -- Merge branch '35-simplify-stages-detail-components-and-services' into 'master'
|\  
| * 1024f09 -- Resolve "Simplify stages detail components and services"
|/  
*   01327da -- Merge branch '36-run-ci-in-cypress-docker-image' into 'master'
|\  
| * bb4397f -- Resolve "Run CI in cypress docker image"
|/  
*   55bf36e -- Merge branch '34-refactor-tsconfig-paths-for-example-app-and-tests' into 'master'
|\  
| * 27b7905 -- Resolve "Refactor tsconfig paths for example app and tests"
|/  
* 664fd49 -- Update project package.json version based on GitLab tag. Done by CI
*   a3781fa -- Merge branch '33-add-secondary-entry-points-to-allow-for-lazy-loading-of-parts-of-the-agenda' into 'master'
|\  
| * 1b0908f -- Resolve "Add secondary entry points to allow for lazy loading of parts of the agenda"
|/  
* 59adceb -- Update project package.json version based on GitLab tag. Done by CI
*   32942e2 -- Merge branch '31-add-request-table-adapter' into 'master'
|\  
| * c748bf9 -- Add dateFormatted to request table adapters
|/  
*   ac1ca48 -- Merge branch '30-replace-kypo-dependencies-with-sentinel' into 'master'
|\  
| * 9a9af98 -- Refactor to use @sentinel deps
|/  
* 0f5b874 -- Update project package.json version based on GitLab tag. Done by CI
*   cce43c2 -- Merge branch '17-refactor-the-way-stages-are-modeled' into 'master'
|\  
| * 833125c -- Resolve "Refactor the way stages are modeled"
|/  
* ddf5f03 -- Update project package.json version based on GitLab tag. Done by CI
*   aabc48b -- Merge branch '28-update-to-angular-10' into 'master'
|\  
| * f7075c0 -- Resolve "Update to Angular 10"
|/  
* 65318b7 -- Update project package.json version based on GitLab tag. Done by CI
