export class PoolDetailRowAdapter {
  id: number;
  unitId: number;
  name: string;
  comment: string;
  lock: string;
  created: Date;
  createdBy: string;
  state: string;
  stages: string[];
}
