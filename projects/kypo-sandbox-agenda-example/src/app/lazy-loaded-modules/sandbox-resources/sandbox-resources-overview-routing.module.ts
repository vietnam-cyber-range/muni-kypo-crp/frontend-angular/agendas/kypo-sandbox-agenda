import { ResourcesPageComponent } from './../../../../../kypo-sandbox-agenda/sandbox-resources/src/components/resources-page.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: ResourcesPageComponent,
  },
];

/**
 * Sandbox definition overview routing
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SandboxResourcesOverviewRoutingModule {}
